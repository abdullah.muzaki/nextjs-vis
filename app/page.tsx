/* eslint-disable react-hooks/exhaustive-deps */
"use client";

import React, { useEffect } from "react";
import { useState } from "react";

import Chart from "chart.js/auto";

const ActivityChart = () => {
  const dataUrls = ["activity_log_2023-08-24.json"];
  const [totalBusyMinutes, setTotalBusyMinutes] = useState(0);

  useEffect(() => {
    async function fetchJsonData(url: string) {
      const response = await fetch(url);
      const data = await response.json();
      return data;
    }

    async function loadAndRenderCharts() {
      const canvas = document.getElementById(
        "activity-chart"
      ) as HTMLCanvasElement | null;

      if (!canvas) return <h1>Canvas element not found</h1>;

      const ctx = canvas.getContext("2d");

      if (!ctx) {
        return <h2>Failed to get canvas context</h2>;
      }

      let totalMinutes = 0; // To calculate the total busy minutes

      for (const dataUrl of dataUrls) {
        const jsonData = await fetchJsonData(dataUrl);

        const timestamps = Object.keys(jsonData);
        const busyCounts = timestamps.map((timestamp) => {
          if (jsonData[timestamp] === "busy") {
            totalMinutes++;
            return 1;
          } else {
            return 0;
          }
        });

        new Chart(ctx, {
          type: "bar",
          data: {
            labels: timestamps,
            datasets: [
              {
                label: "Busy Activity Count",
                data: busyCounts,
                backgroundColor: "blue",
              },
            ],
          },
          options: {
            scales: {
              y: {
                beginAtZero: true,
              },
              x: {
                title: {
                  display: true,
                  text: "Timestamp",
                },
              },
            },
          },
        });
      }

      setTotalBusyMinutes(totalMinutes);
    }

    loadAndRenderCharts();
  }, []);

  function convertMinutesToHoursAndMinutes(minutes: number) {
    const hours = Math.floor(minutes / 60);
    const remainingMinutes = minutes % 60;

    const formattedHours = String(hours).padStart(2, "0");
    const formattedMinutes = String(remainingMinutes).padStart(2, "0");

    return `${formattedHours}:${formattedMinutes}`;
  }

  return (
    <div>
      <div style={{ width: "80%", margin: "0 auto" }}>
        <canvas id="activity-chart"></canvas>
      </div>

      <div
        style={{
          position: "absolute",
          top: "20px",
          right: "20px",
          fontSize: "18px",
        }}
      >
        Total Busy Minutes:{" "}
        <span id="total-busy-minutes">
          {convertMinutesToHoursAndMinutes(totalBusyMinutes)}
        </span>{" "}
        <br />
        Persen:{" "}
        <span id="percentage">
          {((totalBusyMinutes / 480) * 100).toFixed(1)}%
        </span>
      </div>
    </div>
  );
};

export default ActivityChart;
